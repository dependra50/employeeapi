﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAPI.Models
{
    public class EmployeeQualification
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public string EmployeeId { get; set; }
        [Required]
        public int QualificationId { get; set; }
        [Required]
        public int Marks { get; set; }

        public Employee Employee { get; set; }
        public Qualification Qualification { get; set; }
    }
}
