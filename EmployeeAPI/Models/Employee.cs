﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAPI.Models
{
    public class Employee
    {
        [Key]        
        public string Id { get; set; }
        [Required]
        public string EmployeeName { get; set; }
        [Required]
        public string DOB { get; set; }
        [Required(ErrorMessage = "Select Gender")]
        public string Gender { get; set; }
        [Required]
        [Column(TypeName = "decimal(18,4)")]
        public decimal Salary { get; set; }
        [Required]
        public string EntryBy { get; set; }
        [Required]
        public string EntryDate { get; set; }

        public ICollection<EmployeeQualification> EmployeeQualifications { get; set; }
    }
}
