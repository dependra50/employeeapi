﻿using EmployeeAPI.Context;
using EmployeeAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAPI.Repository
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly DataContext _dataContext;

        public EmployeeRepository(DataContext dataContext)
        {
            _dataContext = dataContext ?? throw new ArgumentNullException(nameof(dataContext));
        }

        public async Task<Employee> GetEmployeeById(string employeeId)
        {
            return await _dataContext.Employees.Where(x => x.Id == employeeId)                                                
                                               .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Employee>> GetEmployeeList()
        {
            return await _dataContext.Employees.ToListAsync();


        }

        public async Task<bool> IsAnyEmployeeExist(string name, string dob)
        {
            var result = await _dataContext.Employees.Where(e => e.EmployeeName == name &&
                                                         e.DOB == dob)
                                               .FirstOrDefaultAsync();
            if(result == null)
            {
                return false;
            }

            return true;
        }

        public async Task RegisterEmployee(Employee employee)
        {
            await _dataContext.Employees.AddAsync(employee);
            await _dataContext.SaveChangesAsync();
        }
    }
}
