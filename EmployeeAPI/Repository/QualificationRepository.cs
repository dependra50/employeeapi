﻿using EmployeeAPI.Context;
using EmployeeAPI.Dto;
using EmployeeAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAPI.Repository
{
    public class QualificationRepository : IQualificationRepository
    {
        private readonly DataContext _dataContext;

        public QualificationRepository(DataContext dataContext)
        {
            _dataContext = dataContext ?? throw new ArgumentNullException(nameof(dataContext));
        }

        public async Task<Qualification> GetQualificationById(int id)
        {
            var qualification = await _dataContext.Qualifications.FindAsync(id);
            return qualification;
        }

        public async Task<IEnumerable<QualificationDto>> GetQualificationList()
        {
            return await _dataContext.Qualifications.Select(q => new QualificationDto
            {
                Id = q.Id,
                QualificationName = q.QualificationName
            }).ToListAsync();
        }

        public async Task<bool> IsQualificationExist(string qualification)
        {
            var qualifromRepo = await _dataContext.Qualifications.Where(x => x.QualificationName == qualification)
                                                    .FirstOrDefaultAsync();
            if(qualifromRepo == null)
            {
                return false;
            }
            return true;
        }

        public async Task RegisterQualification(Qualification qualification)
        {
            await _dataContext.Qualifications.AddAsync(qualification);
            await _dataContext.SaveChangesAsync();
        }
    }
}
