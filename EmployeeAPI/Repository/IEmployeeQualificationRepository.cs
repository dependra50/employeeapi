﻿using EmployeeAPI.Dto;
using EmployeeAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAPI.Repository
{
    public interface IEmployeeQualificationRepository
    {
        Task RegisterEmployeeQualification(EmployeeQualification employeeQualification);
        Task<IEnumerable<EmployeeQualificationDto>> GetEmployeeQualificationByEmployeeId(string employeeId);
    }
}
