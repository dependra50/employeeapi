﻿using EmployeeAPI.Dto;
using EmployeeAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAPI.Repository
{
    public interface IQualificationRepository
    {
        Task RegisterQualification(Qualification qualification);
        Task<Qualification> GetQualificationById(int id);
        Task<IEnumerable<QualificationDto>> GetQualificationList();
        Task<bool> IsQualificationExist(string qualification);
    }
}
