﻿using EmployeeAPI.Context;
using EmployeeAPI.Dto;
using EmployeeAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAPI.Repository
{
    public class EmployeeQualificationRepository : IEmployeeQualificationRepository
    {
        private readonly DataContext _dataContext;

        public EmployeeQualificationRepository(DataContext dataContext)
        {
            _dataContext = dataContext ?? throw new ArgumentNullException(nameof(dataContext));
        }

        public async Task<IEnumerable<EmployeeQualificationDto>> GetEmployeeQualificationByEmployeeId(string employeeId)
        {
            return await _dataContext.EmployeeQualifications.Where(eq => eq.EmployeeId == employeeId)
                                                            .Select(s => new EmployeeQualificationDto
                                                            {   
                                                                Id = s.Id,
                                                                Marks = s.Marks,
                                                                QualificationLevel = s.Qualification.QualificationName
                                                            }).ToListAsync();
                                                            
        }

        public async Task RegisterEmployeeQualification(EmployeeQualification employeeQualification)
        {
            await _dataContext.EmployeeQualifications.AddAsync(employeeQualification);
            await _dataContext.SaveChangesAsync();
        }
    }
}
