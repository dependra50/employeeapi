﻿using EmployeeAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAPI.Repository
{
    public interface IEmployeeRepository
    {
        
        Task RegisterEmployee(Employee employee);
        Task<Employee> GetEmployeeById(string employeeId);
        Task<IEnumerable<Employee>> GetEmployeeList();
        Task<bool> IsAnyEmployeeExist(string name, string dob);
    }
}
