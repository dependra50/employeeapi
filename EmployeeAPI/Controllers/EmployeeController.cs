﻿using EmployeeAPI.Dto;
using EmployeeAPI.Models;
using EmployeeAPI.Repository;
using EmployeeAPI.Utilities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IEmployeeQualificationRepository _employeeQualificationRepository;
        private readonly IQualificationRepository _qualificationRepository;

        public EmployeeController(IEmployeeRepository employeeRepository ,
                                  IEmployeeQualificationRepository employeeQualificationRepository ,
                                  IQualificationRepository qualificationRepository)
        {
            _employeeRepository = employeeRepository ?? throw new ArgumentNullException(nameof(employeeRepository));
            _employeeQualificationRepository = employeeQualificationRepository ?? throw new ArgumentNullException(nameof(employeeQualificationRepository));
            _qualificationRepository = qualificationRepository ?? throw new ArgumentNullException(nameof(qualificationRepository));

        }

        
        [HttpPost]
        [Route("registeremployee")]
        public async Task<IActionResult> RegisterEmployee(RegisterEmployeeDto registerEmployeeDto)
        {
            if (ModelState.IsValid)
            {
                var isEmployeeExist = await _employeeRepository.IsAnyEmployeeExist(registerEmployeeDto.EmployeeName, registerEmployeeDto.DOB);
                if (isEmployeeExist)
                {
                    return Ok("Employee Already Exist");

                }
                var uniqueId = string.Concat(UniqueCodeGenerator.GetRandomKey(4),
                                                    UniqueCodeGenerator.GetRandomKey(4));
                

                var employeeToRepo = new Employee
                {
                    Id = uniqueId,
                    Salary = registerEmployeeDto.Salary,
                    DOB = registerEmployeeDto.DOB,
                    EmployeeName = registerEmployeeDto.EmployeeName,
                    EntryBy = registerEmployeeDto.EntryBy,
                    EntryDate = registerEmployeeDto.EntryDate,
                    Gender = registerEmployeeDto.Gender
                };
                await _employeeRepository.RegisterEmployee(employeeToRepo);

                foreach (var qualification in registerEmployeeDto.RegisterEmployeeQualificationDtos)
                {
                    await _employeeQualificationRepository.RegisterEmployeeQualification(new EmployeeQualification
                    {
                        EmployeeId = uniqueId,
                        QualificationId = qualification.QualificationId,
                        Marks = qualification.Marks
                    });
                }

                return Ok("Employee Register Successfully");
            }
            ModelState.AddModelError("", "ModelState Error");
            return BadRequest("ModelState Error");
        }


        [HttpPost]
        [Route("registerqualification")]
        public async Task<IActionResult> RegisterQualification(RegisterQuallificationDto registerQuallificationDto)
        {
            if (ModelState.IsValid)
            {
                var isAny = await _qualificationRepository.IsQualificationExist(registerQuallificationDto.AcademicLevel);
                if (isAny)
                {
                    return BadRequest("Qualification Level Already Exist");
                }

                await _qualificationRepository.RegisterQualification(new Qualification
                {
                    QualificationName = registerQuallificationDto.AcademicLevel
                });

                return Ok("Qualification Register Successfully");

            }
            ModelState.AddModelError("", "ModelState Error");
            return BadRequest("ModelState Error");
        }

        
        [HttpGet]
        [Route("employeebyid/{employeeId}")]
        public async Task<IActionResult> GetEmpoloyeeById(string employeeId)
        {
            var result = await _employeeRepository.GetEmployeeById(employeeId);
            if (result == null)
            {
                return Ok("No Data Found!");
            };
            return Ok(result);

        }

        [HttpGet]
        [Route("employeelist")]
        public async Task<IActionResult> GetEmpoloyeeList()
        {
            var result = await _employeeRepository.GetEmployeeList();
            if (result == null)
            {
                return Ok("No Data Found!");
            };
            return Ok(result);

        }
        [HttpGet]
        [Route("employeequalification/employeeId")]
        public async Task<IActionResult> GetEmployeeQualificationById(string employeeId)
        {
            var result = await _employeeQualificationRepository.GetEmployeeQualificationByEmployeeId(employeeId);
            if (result == null)
            {
                return Ok("No Data Found!");
            };
            return Ok(result);

        }


        [HttpGet]
        [Route("qualificationlist")]
        public async Task<IActionResult> GetQualificationList()
        {
            var result = await _qualificationRepository.GetQualificationList();
            if (result == null)
            {
                return Ok("No Data Found!");
            };
            return Ok(result);

        }

        [HttpGet]
        [Route("qualification/qualificationId")]
        public async Task<IActionResult> GetQualificationById(int qualificationId)
        {
            var result = await _qualificationRepository.GetQualificationById(qualificationId);
            if (result == null)
            {
                return Ok("No Data Found!");
            };
            return Ok(result);

        }


    }
}
