﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAPI.Dto
{
    public class RegisterEmployeeQualificationDto
    {
        public int QualificationId { get; set; }
        public int Marks { get; set; }
    }
}
