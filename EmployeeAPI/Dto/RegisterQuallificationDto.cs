﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAPI.Dto
{
    public class RegisterQuallificationDto
    {
        public string AcademicLevel { get; set; }
    }
}
