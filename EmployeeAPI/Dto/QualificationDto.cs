﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAPI.Dto
{
    public class QualificationDto
    {
        public int Id { get; set; }
        public string QualificationName { get; set; }
    }
}
