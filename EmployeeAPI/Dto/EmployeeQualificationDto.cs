﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAPI.Dto
{
    public class EmployeeQualificationDto
    {
        public int Id { get; set; }
        public string QualificationLevel { get; set; }
        public int Marks { get; set; }
    }
}
