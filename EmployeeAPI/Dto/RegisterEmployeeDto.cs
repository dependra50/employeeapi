﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAPI.Dto
{
    public class RegisterEmployeeDto
    {
        
        [Required]
        public string EmployeeName { get; set; }
        [Required]
        public string DOB { get; set; }
        [Required(ErrorMessage = "Select Gender")]
        public string Gender { get; set; }
        [Required]
        public decimal Salary { get; set; }
        [Required]
        public string EntryBy { get; set; }
        [Required]
        public string EntryDate { get; set; }


        public RegisterEmployeeQualificationDto[] RegisterEmployeeQualificationDtos { get; set; }
    }
}
