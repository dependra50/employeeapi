﻿using EmployeeAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAPI.Context
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Qualification> Qualifications { get; set; }
        public DbSet<EmployeeQualification> EmployeeQualifications { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<EmployeeQualification>(options =>
            {
                options.ToTable("EmployeeQualifications");
                options.HasKey(eq => eq.Id);
                options.HasOne<Employee>(e => e.Employee)
                       .WithMany(eq => eq.EmployeeQualifications)
                       .HasForeignKey(e => e.EmployeeId);
                options.HasOne<Qualification>(q => q.Qualification)
                       .WithMany(eq => eq.EmployeeQualifications)
                       .HasForeignKey(q => q.QualificationId);
            });




        }
    }
}

